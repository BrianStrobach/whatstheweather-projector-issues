package com.mobilevangelist.glass.helloworld;

import android.location.Location;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Josh on 5/18/14.
 */
public class weatherFinder {

    public static String TAG = "WeatherFetcher";

    byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return null;
            }

            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();

            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public String getUrl(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    public String getWeather(Location loc){
        String str = "";
        try {
//            str = new weatherFinder().getUrl("http://api.openweathermap.org/data/2.5/weather?q=Chicago,IL");

//            str = new weatherFinder().getUrl("http://api.openweathermap.org/data/2.5/weather?lat="+loc.getLatitude()+"&lon="+loc.getLongitude());
            str = new weatherFinder().getUrl("http://api.openweathermap.org/data/2.5/weather?lat=42&lon=87");
            Log.i(TAG, "Fetched contents of URL: " + str);
        }catch(IOException ioe) {
            Log.e(TAG, "Failed to fetch URL: " + ioe);
        }
        return str;
    }

    public String getWeather(String chosen){
        String str = "";
        try {
            str = new weatherFinder().getUrl("http://api.openweathermap.org/data/2.5/weather?q="+chosen);
            Log.i(TAG, "Fetched contents of URL: " + str);
        }catch(IOException ioe) {
            Log.e(TAG, "Failed to fetch URL: " + ioe);
        }
        return str;
    }


}